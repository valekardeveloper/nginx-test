echo "Recreating project folder...."
rm -rf project
mkdir project
cd ~/project


################################# get the projects #########################################
echo "Cloning nginx-test project ...."
rm -rf nginx-test
git clone git@bitbucket.org:valekardeveloper/nginx-test.git
##########################################################################################

echo "Instantiating containers...."
cd nginx-test

##############################Remove existing wordpress container#####################

echo "Removing existing wordpress containers...."

wordpress="wordpress"
x=$(docker ps -a | awk -v word=$wordpress '$0~word' )
container_ID=$(echo $x | awk '{print $1}')
image_name=$(echo $x | awk '{print $2}')
echo $container_ID 
echo $image_name
#remove docker container

if [[ ! -z "$container_ID" ]]; then
	docker stop $container_ID
	docker rm $container_ID
	#remove docker image
	docker rmi $image_name
fi

#########################################################################################
#create the containers
cd nginx-proxy
docker-compose up --force-recreate -d
cd .. 
docker-compose up --force-recreate -d
#docker-compose up -d 
##############################################################################################
echo "Deploy script finished execution"
